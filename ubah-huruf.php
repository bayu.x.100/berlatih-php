<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        function ubah_huruf($string){
            $katabaru = "";
            $panjang = strlen($string);
            for ($i=0; $i < $panjang; $i++) { 
                switch ($string[$i]) {
                    case 'a': $huruf = 'b'; break;
                    case 'b': $huruf = 'c'; break;
                    case 'c': $huruf = 'd'; break;
                    case 'd': $huruf = 'e'; break;
                    case 'e': $huruf = 'f'; break;
                    case 'f': $huruf = 'g'; break;
                    case 'g': $huruf = 'h'; break;
                    case 'h': $huruf = 'i'; break;
                    case 'i': $huruf = 'j'; break;
                    case 'j': $huruf = 'k'; break;
                    case 'k': $huruf = 'l'; break;
                    case 'l': $huruf = 'm'; break;
                    case 'm': $huruf = 'n'; break;
                    case 'n': $huruf = 'o'; break;
                    case 'o': $huruf = 'p'; break;
                    case 'p': $huruf = 'q'; break;
                    case 'q': $huruf = 'r'; break;
                    case 'r': $huruf = 's'; break;
                    case 's': $huruf = 't'; break;
                    case 't': $huruf = 'u'; break;
                    case 'u': $huruf = 'v'; break;
                    case 'v': $huruf = 'w'; break;
                    case 'w': $huruf = 'x'; break;
                    case 'x': $huruf = 'y'; break;
                    case 'y': $huruf = 'z'; break;
                    case 'z': $huruf = 'a'; break;
                    default: $huruf = 'salah';break;
                }
                $katabaru = $katabaru.$huruf;
            }
            echo $katabaru;
            echo "<br>";
        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>
</html>